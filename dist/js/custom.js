$(window).load(function(){
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
		$('body').addClass('ios');
	} else{
		$('body').addClass('web');
	};
	$('body').removeClass('loaded'); 
});
/* viewport width */
function viewport(){
	var e = window, 
	a = 'inner';
	if ( !( 'innerWidth' in window ) )
	{
		a = 'client';
		e = document.documentElement || document.body;
	}
	return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
};
/* viewport width */
$(function(){
	/* placeholder*/	   
	$('input, textarea').each(function(){
		var placeholder = $(this).attr('placeholder');
		$(this).focus(function(){ $(this).attr('placeholder', '');});
		$(this).focusout(function(){			 
			$(this).attr('placeholder', placeholder);  			
		});
	});
	/* placeholder*/

	/* mobile-menu */
	$('.btn-mobile').click(function(){
		$(this).toggleClass('btn-mobile_active'), 
		$('.main-nav .nav-list').slideToggle(); 
	});
	
	/* components */
	if( $('.slider-index' ).length) {
		$(".slider-index").owlCarousel({
			loop: true,
			nav: true,
			navText: false,
			dots: false,
			margin: 12,
			responsiveClass: true,
			responsive:{
				0: {
					items: 1,
					slideBy: 1
				},
				370: {
					items: 2,
					slideBy: 2
				},
				650: {
					items: 3,
					slideBy: 3
				},
				960: {
					items: 4,
					slideBy: 4
				},
				1000: {
					items: 5,
					slideBy: 5,
				}
			}
		});
	};
	

});